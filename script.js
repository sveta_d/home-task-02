var n =3;
var m = 3;

function randNumbers() {
    var array = [];
    for (var i = 0; i < n; i++) {
        array[i] = [];
        for (var j = 0; j < m; j++) {
            array[i][j] = (Math.round(-10 + Math.random() * (10 + 10)));
        }
    }
    return array;
}
var array_1 = randNumbers();
var array_2 = randNumbers();

function sum (array1, array2) {
    var array = [];
    for (var i = 0; i < n; i++) {
        array[i] = [];
        for (var j = 0; j < m; j++) {
           array[i][j] = array1[i][j] + array2[i][j];
        }
    }
    return array;
}
var sum_1_2 = sum(array_1, array_2);
var sum_2_1 = sum(array_1, array_2);

function diff(array1, array2){
    var array = [];
    for (var i = 0; i < n; i++) {
       array[i] = [];
        for (var j = 0; j < m; j++) {
            array[i][j] = array1[i][j] - array2[i][j];
        }
    }
    return array;
}
var diff_1_2 = diff(array_1, array_2);
var diff_2_1 = diff(array_2, array_1);

function multiplication(array1, array2) {
    var array = [];
    for (var i = 0; i < n; i++) {
        array[i] = [];
        for (var j = 0; j < m; j++) {
            var result = 0;
            for (var n = 0; n < 3; n++) {
                result += array1[j][n] * array2[n][i];
            }
           array[j][i] = result;
        }
    }
    return array;
}
var product_1_2 = multiplication(array_1, array_2);
var product_2_1  = multiplication(array_2, array_1);

function findDeterminant(array) {
    return array[0][0] * array[1][1] * array[2][2] + array[0][1] * array[1][2] * array[2][0] + array[1][0] * array[2][1] *
        array[0][2] - array[0][2] * array[1][1] * array[2][0] - array[1][0] * array[0][1] * array[2][2] - array[1][2] *
        array[2][1] * array[0][0];
}
var determinant_1 = findDeterminant(array_1);
var determinant_2 = findDeterminant(array_2);

function transposing(array) {
    var result = [];
    for (var i = 0; i < n; i++) {
        result[i] = [];
        for (var j = 0; j < m; j++) {
           result[i][j] = array[j][i];
        }
    }
    return result;
}
var transpose_1 = transposing(array_1);
var transpoae_2 = transposing(array_2);

